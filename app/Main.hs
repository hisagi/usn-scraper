{-# LANGUAGE OverloadedStrings #-}

module Main where

import Text.HTML.Scalpel hiding (URL)
import Control.Applicative
import Control.Monad
import Text.Printf

type Title = String
type URL = String

data Record
    = Record Title URL

instance Show Record where 
    show (Record title url) =
        printf "%s (%s)" title url

allRecords :: IO (Maybe [Record])
allRecords = scrapeURL "https://usn.ubuntu.com" records
    where

        records :: Scraper String [Record]
        records = chroots ("div"  @: [hasClass "col-8"]) record

        record :: Scraper String Record
        record = usnRecord <|> usnRecord

        usnRecord :: Scraper String Record
        usnRecord = do
            title <- text $ "h3" @: [hasClass "p-heading--four"] // "a"
            url <- attr "href" $ "h3" @: [hasClass "p-heading--four"] // "a"
            return $ Record title url

main :: IO ()
main = print =<< allRecords
